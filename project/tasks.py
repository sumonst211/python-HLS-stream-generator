import json
import string
import random
import calendar
import time
import subprocess
import re
import gridfs
import os
import glob
import requests
import traceback
from io import BytesIO  #check_url_file
import pycurl           #check_url_file
import hashlib          #check_url_file
import uuid                                     #save_video_from_url 
import time                                     #save_video_from_url
#from pySmartDL import SmartDL as SmartDL_object
from download import *
from log import do_print

import datetime
import sys, os

from PIL import Image, ImageStat
from os.path import splitext, split, join, dirname, basename
from pymongo import MongoClient
from celery import Task
from urllib.parse import urlparse #save_video_from_url 


from extensions import celery_app
from config import DefaultConfig,DevelopmentConfig

import exc  #check_url_file
from ffmpy import FFmpeg


db = MongoClient(
    host=os.environ.get('MONGO_HOST'),
    port=int(os.environ.get('MONGO_PORT', 27017)),
    connect=False
).video_optimizer


##################################
#          Call Backs
##################################

class save_video_callback(Task):
    def on_success(self, retval, task_id, args, kwargs):
        print(f"on_success task_id = {task_id}\n")
        task = video_converter.apply_async(
            kwargs=dict(
                input_file=retval['source_filepath'],
                base_filename=retval['base_filename'],
                watermark=retval['watermark'],
                client_ip=retval['client_ip'],
                webhook=retval['webhook']
                ),
            task_id=task_id
            )
        pass

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        print(f"on_failure task_id = {task_id}\n")
        print(f"on_failure einfo = {einfo}\n")
        pass

class CallbackTask(Task):
    def on_success(self, retval, task_id, args, kwargs):
        fs = gridfs.GridFS(db)

        base_filename = retval['base_filename']
        converted_filepath = retval['video_file']
        meta = get_meta_info(converted_filepath)

        seconds = retval['seconds']
        screenshot_filepath = retval['screenshot_file']
        client_ip = retval['client_ip']
        webhook = retval['webhook']

        key = ''.join(random.SystemRandom().choice(
            string.ascii_uppercase +
            string.digits +
            string.ascii_lowercase) for _ in range(32))

        video_file_id = fs.put(
            open(converted_filepath, 'rb'),
            key=key,
            task_id=task_id,
            base_filename=base_filename,
            filename=task_id + '.mp4',
            type='video',
            seconds=seconds,
            clientIP=client_ip,
            webhook=webhook,
            meta=meta,
        )
        # print('VideoFileID: {}'.format(video_file_id))
        # print('Key: {}'.format(key))

        screenshot_file_id = fs.put(
            open(screenshot_filepath, 'rb'),
            key=key,
            task_id=task_id,
            filename=task_id + '.jpg',
            type='screenshot',
        )
        # print('ScreenshotFileID: {}'.format(screenshot_file_id))
        # print('Key: {}'.format(key))

        filepath = dirname(converted_filepath)
        converted_filename = basename(converted_filepath)

        files_pattern = join(
            filepath,
            '*{pattern}*'.format(
                pattern=converted_filename.replace('convert-', '').replace('.mp4', '')
            )
        )
        for f in glob.glob(files_pattern):
            os.remove(f)

        doc = fs.get(video_file_id)._file

        server_address = '{protocol}://{host}'.format(
            protocol=DefaultConfig.SERVER_PROTOCOL,
            host=DefaultConfig.SERVER_HOST
        )
        send_callback_request(
            webhook=webhook,
            json_data={
                'task_id': task_id,
                'status': 'OK',
                'key': str(doc['_id']),
                'timestamp_now': calendar.timegm(time.gmtime()),
                'file': {
                    'base_filename': doc['base_filename'],
                    'md5': doc['md5'],
                    'size': doc['length'],
                    'seconds': doc['seconds'],
                    'date_upload': str(doc['uploadDate']),
                },
                'meta': meta,
                '_link': {
                    'video': '{}/pull/{}?key={}'.format(
                        server_address, video_file_id, doc['key']),
                    'screenshot': '{}/pull/{}?key={}'.format(
                        server_address, screenshot_file_id, doc['key']),
                }
            },
            file_object_id=video_file_id
        )

        print('*** Task {} Successfully Completed ***'.format(task_id))

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        # TODO: send file_object_id
        send_callback_request(
            webhook=kwargs['webhook'],
            json_data={
                'task_id': task_id,
                'status': 'FAILED',
                'timestamp_now': calendar.timegm(time.gmtime()),
                'exception': str(exc),
                'traceback': traceback.format_exc()
            },
            file_object_id=None
        )


##################################
#          Tasks
##################################

@celery_app.task(base=CallbackTask, bind=True, name='video_converter')
def video_converter(self, base_filename, input_file, watermark, client_ip, webhook):
    do_print(f"video_converter _______START________")
    path, filename = split(input_file)
    orig_filename, file_ext = splitext(filename)
    output_file = join(path, 'convert-' + orig_filename + '.mp4')
    screenshot_file = join(path, orig_filename + '.jpg')

    do_print('_________1_____________')
    pixel_padding = DefaultConfig.WATERMARK_PADDING

    do_print('_________2_____________')
    overlay_dict = {
        'tl': '"overlay={}:{}"'.format(pixel_padding, pixel_padding),
        'tr': '"overlay=W-w-{}:{}"'.format(pixel_padding, pixel_padding),
        'bl': '"overlay={}:H-h-{}"'.format(pixel_padding, pixel_padding),
        'br': '"overlay=W-w-{}:H-h-{}"'.format(pixel_padding, pixel_padding)
    }

    do_print('_________3_____________')
    overlay = overlay_dict.get(watermark)

    '''
    overlay_option = ''
    if overlay:
        overlay_option = '-i watermark.png -filter_complex {} '.format(overlay)

    do_print('_________4_____________')
    options = overlay_option + '-vf scale=-2:{} -vcodec h264 -acodec aac -strict -2'.format(
        DefaultConfig.OUTPUT_VIDEO_HEIGHT
    )
    '''



    '''
    cmd_convert = f'ffmpeg -y -i {input_file} {options} {output_file}'
    do_print(f'cmd_convert\n{cmd_convert}')
    process = subprocess.Popen(cmd_convert,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               universal_newlines=True,
                               shell=True)
    
    
    '''


    ffmpeg_inputs_dict = {input_file: None}
    if overlay:
        ffmpeg_inputs_dict['watermark.png'] = f'-filter_complex {overlay}'

    ffmpeg_outputs_dict = {
        output_file: f'-vf scale=-2:{DefaultConfig.OUTPUT_VIDEO_HEIGHT} -vcodec h264 -acodec aac -strict -2'
                           }

    ff_convert = FFmpeg(
        inputs=ffmpeg_inputs_dict,
        outputs=ffmpeg_outputs_dict
    )
    do_print(f'________ff_convert.cmd___________{ff_convert.cmd}')
    ff_convert.run()

    duration_match = None
    fps_match = None

    hours = None
    minutes = None
    seconds = None
    fps = None
    frames = None

    do_print('_________5_____________')
    for line in process.stdout:
        do_print(f'________line in process.stdout________')
        do_print(f'________line\n{line}\n________________')
        if not duration_match:
            pattern = 'Duration:\s+(\d{2}):(\d{2}):([-+]?[0-9]*\.?[0-9]+.), start:'
            # pattern = 'Duration:\s+(\d{2}):(\d{2}):(\d{2}), start:'
            duration_match = re.search(pattern, line)
            if duration_match:
                hours = int(duration_match.groups()[0])
                do_print(f'__________duration_match 1 : {duration_match.groups()[1]}______')
                minutes = int(duration_match.groups()[1])
                do_print(f'__________minutes : {minutes}______')
                seconds = int(duration_match.groups()[2][:2])
        if not fps_match:
            pattern = '(\d+) tbr,'
            fps_match = re.search(pattern, line)
            if fps_match:
                fps = int(fps_match.groups()[0])
        
        if seconds is not None and fps is not None:
            frames = (hours * 3600 + minutes * 60 + seconds) * fps

        if frames:
            pattern = 'frame=\s+(\d+) fps'
            current_frame_match = re.search(pattern, line)
            if current_frame_match:
                current_frame = int(current_frame_match.groups()[0])
                percent = str(current_frame / frames)[2:4]

                self.update_state(state='PROGRESS',
                                  meta={'percent': percent,
                                        'status': 'In Progress'})

    do_print('_________6_____________')
    take_screenshot(minutes, seconds, output_file, screenshot_file)
    do_print('_________6-1_____________')
    for i in range(3):
        if is_good_screenshot(screenshot_file):
            break
        take_screenshot(minutes, seconds, output_file, screenshot_file)

    do_print('_________7_____________')
    return {
        'percent': 100,
        'status': 'Completed',

        'base_filename': base_filename,
        'video_file': output_file,
        'seconds': hours * 3600 + minutes * 60 + seconds,

        'screenshot_file': screenshot_file,

        'client_ip': client_ip,
        'webhook': webhook
    }

@celery_app.task(base=save_video_callback, bind=True, name='save_video_from_url')
def save_video_from_url(self, url , base_filename , watermark , client_ip , webhook, config_url_cache, config_media_folder):
    do_print(f"*********************WRITE LOG**********************")
    try:
        do_print(f"url ={url}")
        check_url_file(url)
        md5sum(url)
        disassembled = urlparse(url)
        orig_filename, file_ext = splitext(basename(disassembled.path))
        do_print(f"orig_filename ={orig_filename}")
        do_print(f"file_ext ={file_ext}")

        file_exists = False
        if config_url_cache:
            saved_filename = md5sum(url) + file_ext
            source_filepath = config_media_folder + saved_filename
            if os.path.isfile(source_filepath):
                file_exists = True
        else:
            do_print(f"not url cache")
            saved_filename = str(uuid.uuid4()) + file_ext

        source_filepath = config_media_folder + saved_filename
        do_print(f"source_filepath ={source_filepath}")

        if not file_exists:
            do_print("file not exists")
            download(url, path=source_filepath)
#
#
#
#            obj = SmartDL([url], progress_bar=True)
#            do_print(obj)
#            obj.start(blocking=False)
#            do_print(obj.isFinished())
#            do_print(obj.isSuccessful())
#            while not obj.isFinished():
#                do_print("is not finished")
#                do_print("Speed: %s" % obj.get_speed(human=True))
#                do_print("Already downloaded: %s" % obj.get_dl_size(human=True))
#                do_print("Eta: %s" % obj.get_eta(human=True))
#                do_print("Progress: %d%%" % (obj.get_progress()*100))
#                do_print("Progress bar: %s" % obj.get_progress_bar())
#                do_print("Status: %s" % obj.get_status())
#                do_print("\n"*2+"="*50+"\n"*2)
#                time.sleep(0.2)

#            if obj.isSuccessful():
#                do_print("downloaded file to '%s'" % obj.get_dest())
#                do_print("download task took %ss" % obj.get_dl_time(human=True))
#                do_print("File hashes:")
#                do_print(" * MD5: %s" % obj.get_data_hash('md5'))
#                do_print(" * SHA1: %s" % obj.get_data_hash('sha1'))
#                do_print(" * SHA256: %s" % obj.get_data_hash('sha256'))
#            else:
                #do_print("Download failed with the following exceptions:")
                #for e in obj.get_errors():
                #        do_print(unicode(e))
                #do_print("There were some errors:")
#                for e in obj.get_errors():
#                        do_print(str(e))
                # TODO : status taghir konad
#                raise exc.FileNotDownloadException


            #cmd_download = 'wget --no-check-certificate -O {filepath} {url}'.format(
            #    filepath=source_filepath, url=url)
            # print(cmd_download)
            #do_print(f"cmd_download ={cmd_download}")
            #download_process = subprocess .Popen(
            #    cmd_download,
            #    stdout=subprocess.PIPE,
            #    stderr=subprocess.STDOUT,
            #    shell=True)
            #do_print(f"download_process ={download_process}")
            #for line in download_process.stdout:
            #    do_print(f"line = {line}")
            # TODO : percent mohasebeh shavad
            # self.update_state(state='PROGRESS',
            #                     meta={'percent': percent,
            #                         'status': 'Downloading'})
            #download_process_result = download_process.communicate()
            #do_print(f"download_process_result ={download_process_result}")
            #if ' failed: ' in download_process_result[0].decode('iso-8859-1'):
            # TODO : status taghir konad
            #    raise exc.FileNotDownloadException
            do_print(f"download _______OK________")
        return {
            'percent': 100,
            'status': 'Completed',

            'source_filepath': source_filepath,
            'base_filename':base_filename,
            'watermark': watermark,
            'client_ip': client_ip,
            'webhook': webhook
        }
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        errno, strerror = e.args
        do_print(f"""
                ERROR = {e} \n
                errorType={exc_type}\n
                filename={fname} \n
                line={exc_tb.tb_lineno} \n
                errno={errno} \n
                strerror={errno}
                """)
        if errno==6 : # Could not resolve host
            pass
    finally:
        # optional clean up code
        pass




##################################
#         Local Functions
##################################

def md5sum(string):
    m = hashlib.md5()
    m.update(string.encode('utf-8'))
    return m.hexdigest()

def check_url_file(url):
    buffer = BytesIO()

    c = pycurl.Curl()
    c.setopt(c.URL, url)
    c.setopt(c.HEADER, True)
    c.setopt(c.NOBODY, True)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()

    response_code = c.getinfo(c.RESPONSE_CODE)
    # total_time = c.getinfo(c.TOTAL_TIME)
    c.close()

    if response_code == 404:
        raise exc.FileNotFoundException

    if response_code >= 400:
        raise exc.FileNotValidException

def send_callback_request(webhook, json_data, file_object_id=None):
    for i in range(DefaultConfig.RETRY_CALLBACK_REQUEST_COUNT):
        try:
            response = requests.post(webhook, json=json_data)
            if file_object_id is not None:
                db.fs.files.update_one(
                    {'_id': file_object_id},
                    {'$set': {'callback_response': response.reason}})
            if 200 <= response.status_code <= 299:
                return True
        except requests.exceptions.ConnectionError:
            # TODO: CHECK CELERY KILL
            if file_object_id is not None:
                db.fs.files.update_one(
                    {'_id': file_object_id},
                    {'$set': {'callback_response': 'ConnectionError'}})
                print('ConnectionError- FileID: {}'.format(file_object_id))
            print('****************** NOK')
            print(webhook)

        time.sleep(2 ** (i + 2))

    return False

def take_screenshot(minutes, seconds, input_file, screenshot_file):
    do_print(f"take_screenshot_______1________")
    if seconds == 0:
        second_position = '00'
    else:
        do_print(f"take_screenshot_______2________")
        do_print(f'________minutes________{minutes}')
        max_second = 59 if minutes >= 1 else seconds
        second_position = str(random.randint(1, max_second))

    position = '00:00:' + second_position
    do_print(f"take_screenshot_______3________")
    cmd_screenshot = 'ffmpeg -y -ss {position} -i {input_file} -vframes 1 -q:v 2 {screenshot_file}'.format(
        position=position, input_file=input_file, screenshot_file=screenshot_file)
    do_print(f"___________cmd_screenshot____________{cmd_screenshot}")
    do_print(f"take_screenshot_______4________")
    process = subprocess.Popen(cmd_screenshot,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               shell=True)
    do_print(f"take_screenshot_______5________")
    process.communicate()

def is_good_screenshot(image_path):
    do_print('_________6-3_____________')
    do_print(f'_____image_path_______{image_path}')
    im = Image.open(image_path).convert('L')
    do_print(f'_____im____{im}_____________')
    stat = ImageStat.Stat(im)
    do_print(f'_____stat____{stat}_____________')
    min_value, max_value = stat.extrema[0]
    do_print('_________6-4_____________')
    do_print(f'_____max_value____{max_value}_____________')
    do_print(f'______min_value___{min_value}_____________')
    return False if max_value - min_value < 50 else True

def purge(folder, pattern):
    for f in os.listdir(folder):
        if re.search(pattern, f):
            os.remove(os.path.join(folder, f))

def get_meta_info(filename):
    cmd_get_meta = "exiftool -j {filename}".format(filename=filename)
    process = subprocess.Popen(cmd_get_meta,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               shell=True)
    raw_result = process.communicate()
    meta = json.loads(raw_result[0])[0]

    pop_keys = ['SourceFile', 'FileName', 'Directory', 'FilePermissions']
    for key in pop_keys:
        if key in meta.keys():
            meta.pop(key)

    return meta

