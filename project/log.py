import datetime
from config import DevelopmentConfig

def do_print(print_value):
    log_file = open(DevelopmentConfig.TASK_LOG_PATH,'a')
    log_file.write(f"\n{datetime.datetime.now():%Y-%m-%d %H:%M:%S} TASK: "+print_value+"\n")